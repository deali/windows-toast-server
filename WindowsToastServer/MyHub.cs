﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace WindowsToastServer {
    //默认访问的hub名是首字母变小写的名称，如果没有HubName的注解的话，这个hub的名是myHub
    //[HubName("DataHub")]
    class MyHub : Hub {
        [HubMethodName("send")]
        public void Send(string name, string message) {
            Clients.All.addMessage(name, message);
            Clients.Caller.addMessage(name, message);

            Console.WriteLine($"{name}/{message}");
        }
    }
}
