﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;

using Owin;

[assembly: OwinStartup(typeof(WindowsToastServer.Startup))]
namespace WindowsToastServer {
    public class Startup {
        public void Configuration(IAppBuilder app) {
            // 有关如何配置应用程序的详细信息，请访问 https://go.microsoft.com/fwlink/?LinkID=316888
            // 启用跨域支持
            app.UseCors(CorsOptions.AllowAll);


            app.MapSignalR(new HubConfiguration {
                EnableDetailedErrors = true
            });

            Console.WriteLine("启动SignalR");
        }
    }
}
