﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Owin.Hosting;

namespace WindowsToastServer {
    public partial class FormMain : Form {
        private const string signalrUrl = "http://localhost:8082";
        private readonly IDisposable signalr;

        public FormMain() {
            InitializeComponent();

            signalr = WebApp.Start(signalrUrl);
        }

        private void buttonMin_Click(object sender, EventArgs e) {
            notifyIcon.Icon = this.Icon;
            this.ShowInTaskbar = false;
            this.Hide();

            notifyIcon.BalloonTipText = "test";
            notifyIcon.ShowBalloonTip(5);
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e) {
            this.Visible = !this.Visible;
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e) {
            signalr.Dispose();
        }
    }
}